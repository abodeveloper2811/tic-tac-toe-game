import React, {useState} from "react";

import {Board} from "./components/Board";
import {ResetButton} from "./components/ResetButton";
import {ScoreBoard} from "./components/ScoreBoard";
import './App.css';
import "./components/game.css"
import "./components/game-result.css"
import Logo from "./img/logo.png";
import Rock from "./img/Rock.png";
import Paper from "./img/Paper.png";
import Scissors from "./img/Scissors.png";
import {toast, ToastContainer} from "react-toastify";

const App = () => {

  const WIN_CONDITIONS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  const [xPlaying, setXPlaying] = useState(true);
  const [board, setBoard] = useState(Array(9).fill(null));
  const [scores, setScores] = useState({ xScore: 0, oScore: 0 });
  const [gameOver, setGameOver] = useState(false);
  const [count, setCount] = useState(0);

  const handleBoxClick = (boxIdx) => {

    // Step 1: Update the board
    const updatedBoard = board.map((value, idx) => {
      if (idx === boxIdx) {
        return xPlaying ? "X" : "O";
      } else {
        return value;
      }

    });

    setBoard(updatedBoard);

    // Step 2: Check if either player has won the game
    const winner = checkWinner(updatedBoard);

    if (winner) {
      if (winner === "O") {
        let { oScore } = scores;
        oScore += 1;
        setScores({ ...scores, oScore });
        toast.success("Scissors won !!!");
      } else {
        let { xScore } = scores;
        xScore += 1;
        setScores({ ...scores, xScore });
        toast.success("Rock won !!!");
      }
    }

    setCount(count+1);
    console.log(count);

    if (count === 8){
      toast.success('Paper !!!');
      setTimeout(()=>{
        reStart();
      },2000);
    }

    // Step 3: Change active player
    setXPlaying(!xPlaying);
  };

  const checkWinner = (board) => {
    for (let i = 0; i < WIN_CONDITIONS.length; i++) {
      const [x, y, z] = WIN_CONDITIONS[i];

      // Iterate through win conditions and check if either player satisfies them
      if (board[x] && board[x] === board[y] && board[y] === board[z]) {
        setTimeout(()=>{
          setGameOver(true);
          setCount(0);
        }, 1000);
        return board[x];
      }
    }
  };

  const resetBoard = () => {
    setXPlaying(true);
    setGameOver(false);
    setBoard(Array(9).fill(null));
  };

  const reStart = () =>{
    setXPlaying(true);
    setGameOver(false);
    setBoard(Array(9).fill(null));
  };

  const GameResult = () =>{
    return(
        <div className="game-result">
          <div className="logo-box">
            <img src={Logo} alt=""/>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-md-4 offset-lg-4">

                <div className="game-over-text">GAME OVER</div>

                <div className="top-img-box">
                  <img src={Rock} alt=""/>
                  <img src={Paper} alt=""/>
                  <img src={Scissors} alt=""/>
                </div>

                <div className="result-box">
                  <div className="result">
                    <div className="img-box">
                      <img src={Rock} alt=""/>
                    </div>
                    <div className="number">{scores.xScore}</div>
                  </div>
                  <div className="vs">vs</div>
                  <div className="result">
                    <div className="img-box">
                      <img src={Scissors} alt=""/>
                    </div>
                    <div className="number">{scores.oScore}</div>
                  </div>
                </div>

                <button onClick={reStart} className="restart-btn">
                  RESTART
                </button>

              </div>
            </div>
          </div>

        </div>
    )
  };

  const Games = () => {
    return (
        <div className="game-container">

          <div className="logo-box">
            <img src={Logo} alt=""/>
          </div>

          <ScoreBoard scores={scores} xPlaying={xPlaying} />
          <Board board={board} onClick={gameOver ? resetBoard : handleBoxClick} />
          <ResetButton resetBoard={resetBoard} />

        </div>
    );
  };

  return (
    <div className="App">

      {
        gameOver ?
            <GameResult/> :
            <Games/>
      }

      <ToastContainer/>
    </div>
  );
};

export default App;


