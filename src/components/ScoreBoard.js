import React from 'react';
import Rock from "../img/Rock.png"
import Scissors from "../img/Scissors.png"

import "./ScoreBoard.css"

export const ScoreBoard = ({ scores, xPlaying }) => {
  const { xScore, oScore } = scores;

  return (
    <div className="scoreboard">
      <span className={`score x-score ${!xPlaying && "inactive"}`}>
          <img src={Rock} alt=""/> - {xScore}
      </span>
      <span className={`score o-score ${xPlaying && "inactive"}`}>
          <img src={Scissors} alt=""/> - {oScore}
      </span>
    </div>
  )
};
